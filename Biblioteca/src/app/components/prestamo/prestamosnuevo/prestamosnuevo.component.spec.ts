import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestamosnuevoComponent } from './prestamosnuevo.component';

describe('PrestamosnuevoComponent', () => {
  let component: PrestamosnuevoComponent;
  let fixture: ComponentFixture<PrestamosnuevoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrestamosnuevoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestamosnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

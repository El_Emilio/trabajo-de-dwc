import { Injectable } from '@angular/core';
import{AngularFirestore} from '@angular/fire/compat/firestore'

@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  constructor(private firestore: AngularFirestore) {}
   //Crea un nuevo libro
   public createLibro(data: {nombre: string, autor: string, foto:string, isbn:number, genero:string, descripcion:string, prestado:boolean}) {
    return this.firestore.collection('libros').add(data);
  }
  //Obtiene un libro
  public getLibro(documentId: string) {
    return this.firestore.collection('libros').doc(documentId).snapshotChanges();
  }
 
  //Obtiene todos los libros
  public getLibros() {
    return this.firestore.collection('libros').snapshotChanges();
  }
   //Actualiza un libros
   public updateLibros(documentId: any, data: any) {
    return this.firestore.collection('libros').doc(documentId).set(data);
  }
  //Actualiza un libro
  public updateLibro(documentId: string, data: any) {
    return this.firestore.collection('libros').doc(documentId).set(data);
  }

  //Eliminar un libro
  public deleteLibro(DocumentId:string){
    return this.firestore.collection("libros").doc(DocumentId).delete();
  }
  public getLibrosFiltro() {
    return this.firestore.collection('libros').snapshotChanges();
    
  }
  public obtenerLibrosFalse(){
    return  this.firestore.collection('libros', ref =>ref.where ('prestado',"==",false )).snapshotChanges();
  }

}



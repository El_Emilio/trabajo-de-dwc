import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ParamMap, Router, ActivatedRoute } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-prestamosnuevo',
  templateUrl: './prestamosnuevo.component.html',
  styleUrls: ['./prestamosnuevo.component.css']
})
export class PrestamosnuevoComponent implements OnInit {
  public libros: any = [];
  public socios: any;
  librito:any;
  public picker: any = [];
  editaForm: any;
  fechaHoy:Date = new Date();
  libroElegido: any;
  documentId: any;
  constructor(private asf: PrestamosService,private rutita:ActivatedRoute ,private route: Router, private librosServi: LibrosService, private sociosServi: SociosService, private router: Router) { }

  ngOnInit(): void {
    this.librosServi.obtenerLibrosFalse().subscribe((libroSnapshots) => {
      this.libros = [];
      libroSnapshots.forEach((libroData: any) => {
        this.libros.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data(),
        });
      })
      console.log(this.libros);
    })


    this.sociosServi.getSocios().subscribe((socioSnapshots) => {
      this.socios = [];
      socioSnapshots.forEach((socioData: any) => {
        this.socios.push({
          id: socioData.payload.doc.id,
          data: socioData.payload.doc.data(),
        });
      })
    })
  }


  public currentStatus = 1;
  public nuevoPrestamoForm = new FormGroup({
    libro: new FormControl(''),
    socio: new FormControl(''),
    fechaInicio: new FormControl(this.fechaHoy),
    fechaFin: new FormControl('', Validators.required),


  });
  public newPrestamo(form: any, documentId = this.documentId) {
    console.log(`Status: ${this.currentStatus}`);
    if (this.currentStatus == 1) {
      let data = {
        libro: form.libro,
        socio: form.socio,
        fechaInicio: form.fechaInicio,
        fechaFin: form.fechaFin

        
      }
      this.asf.createPrestamo(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.nuevoPrestamoForm.setValue({
          libro: '',
          socio: '',
          fechaInicio: '',
          fechaFin: '',

        });
      }, (error) => {
        console.error(error);
      });

      let bucle =this.librosServi.getLibro(form.libro).subscribe((libroData:any)=>{
        this.libroElegido = libroData.payload.data();
        console.log(this.libroElegido.prestado);
        this.libroElegido.prestado = true;
        this.librosServi.updateLibro(form.libro, this.libroElegido);
        bucle.unsubscribe();
      })

    } else {
      let data = {
        libro: form.libro,
        socio: form.socio,
        fechaInicio: form.fechaInicio,
        fechaFin: form.fechaFin

      }
      this.asf.updatePrestamos(documentId, data).then(() => {
        this.currentStatus = 1;
        this.nuevoPrestamoForm.setValue({
          libro: '',
          socio: '',
          fechaInicio: '',
          fechaFin: ''

        });
        
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });

    }

      
      
    this.route.navigate(["/dashboard/prestado"])
  }/*
  libre(idLibro:String): Boolean{
    this.librosServi.getLibro(String (idLibro))
    
   return Boolean(idLibro.)
  }*/

}

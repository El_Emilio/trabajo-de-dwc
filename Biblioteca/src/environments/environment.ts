// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDGgy4-LYwrUbz5lip9iSZDSc6HOESHLKs",
    authDomain: "biblioteca-2436b.firebaseapp.com",
    projectId: "biblioteca-2436b",
    storageBucket: "biblioteca-2436b.appspot.com",
    messagingSenderId: "83709798858",
    appId: "1:83709798858:web:60e876ee50b53d8acc8733"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

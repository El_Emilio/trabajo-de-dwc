import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class PrestamosService {

  constructor(private firestore: AngularFirestore) { }
     //Crea un nuevo prestamo
     public createPrestamo(data: {libro: string, socio: string, fechaInicio:Date, fechaFin:Date}) {
      return this.firestore.collection('prestamos').add(data);
    }
    //Obtiene un prestamo
    public getPrestamo(documentId: string) {
      return this.firestore.collection('prestamos').doc(documentId).snapshotChanges();
    }
    //Obtiene todos los Prestamos
    public getPrestamos() {
      return this.firestore.collection('prestamos').snapshotChanges();
    }
     //Actualiza un Prestamos
     public updatePrestamos(documentId: any, data: any) {
      return this.firestore.collection('prestamos').doc(documentId).set(data);
    }
  
    //Actualiza un Prestamo
    public updatePrestamo(documentId: string, data: any) {
      return this.firestore.collection('prestamos').doc(documentId).set(data);
    }
  
    //Eliminar un Prestamo
    public deletePrestamo(DocumentId:string){
      return this.firestore.collection("prestamos").doc(DocumentId).delete();
    }
    
  
}

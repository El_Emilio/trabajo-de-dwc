import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';

@Component({
  selector: 'app-libros-edit',
  templateUrl: './libros-edit.component.html',
  styleUrls: ['./libros-edit.component.css']
})
export class LibrosEditComponent implements OnInit {
  documentId:any;
  editaForm:any
  libroElegido:any;
  constructor(private librosServi:LibrosService, private rutita:ActivatedRoute, private router:Router) { }

  ngOnInit(): void { 
    this.rutita.paramMap.subscribe((params:ParamMap)=>{
      this.documentId=(<string>params.get('id'));
      this.librosServi.getLibro(this.documentId).subscribe((libroData:any)=>{
        this.libroElegido=libroData.payload.data();
        this.editaForm = new FormGroup({
          nombre: new FormControl(this.libroElegido.nombre, Validators.required),
          autor: new FormControl(this.libroElegido.autor, Validators.required),
          foto: new FormControl(this.libroElegido.foto, Validators.required),
          isbn: new FormControl(this.libroElegido.isbn, Validators.required),
          genero: new FormControl(this.libroElegido.genero, Validators.required),
          descripcion: new FormControl(this.libroElegido.descripcion, Validators.required),
          prestado: new FormControl('false'),
      
        });
      })
    })
  }
   editarLibro(){
     if(this.editaForm.valid){
       this.librosServi.updateLibro(this.documentId, this.editaForm.value);

     }
     this.router.navigate(['/dashboard/libritos'])
   }
}

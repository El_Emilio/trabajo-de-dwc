import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-socios-edit',
  templateUrl: './socios-edit.component.html',
  styleUrls: ['./socios-edit.component.css']
})
export class SociosEditComponent implements OnInit {

  documentId:any;
  editaForm:any
  socioElegido:any;
  constructor(private sociosServi:SociosService, private rutita:ActivatedRoute, private router:Router) { }

  ngOnInit(): void { 
    this.rutita.paramMap.subscribe((params:ParamMap)=>{
      this.documentId=(<string>params.get('id'));
      this.sociosServi.getSocio(this.documentId).subscribe((socioData:any)=>{
        this.socioElegido=socioData.payload.data();
        this.editaForm = new FormGroup({
          nombre: new FormControl(this.socioElegido.nombre, Validators.required),
          apellidos: new FormControl(this.socioElegido.apellidos, Validators.required),
          foto: new FormControl(this.socioElegido.foto, Validators.required),
          edad: new FormControl(this.socioElegido.edad, Validators.required),
          correo: new FormControl(this.socioElegido.correo, Validators.required),
          telefono: new FormControl(this.socioElegido.telefono, Validators.required),
          generofav: new FormControl(this.socioElegido.telefono, Validators.required),
          tienelibro: new FormControl('false'),
        });
      })
    })
  }
   editarSocio(){
     if(this.editaForm.valid){
       this.sociosServi.updateSocio(this.documentId, this.editaForm.value);

     }
     this.router.navigate(['/dashboard/ssocios'])
   }

}

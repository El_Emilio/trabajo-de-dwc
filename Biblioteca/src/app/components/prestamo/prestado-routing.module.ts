import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { PrestamosnuevoComponent } from './prestamosnuevo/prestamosnuevo.component';

const routes: Routes = [
  {path:"", component:PrestamosComponent},
  {path:"nuevoPrestamo", component:PrestamosnuevoComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestadoRoutingModule { }

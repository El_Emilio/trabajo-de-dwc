import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibritosRoutingModule } from './libritos-routing.module';
import { LibrosEditComponent } from './libros-edit/libros-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [

    LibrosEditComponent
  ],
  imports: [
    CommonModule,
    LibritosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    
  ]
})
export class LibritosModule { }

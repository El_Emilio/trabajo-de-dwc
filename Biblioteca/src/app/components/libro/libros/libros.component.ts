import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LibrosService } from 'src/app/services/libros/libros.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {
  public libros:any=[];
  public documentId = "";
  public currentStatus = 1;
  term!:string;
  constructor(private librosServi:  LibrosService ) { }

  dataSource:any; 
  ngOnInit(): void {
    this.librosServi.getLibros().subscribe( (libroSnapshots)=>{
      this.libros=[];
      libroSnapshots.forEach((libroData: any)=>{
        this.libros.push({
          id:libroData.payload.doc.id,
          data:libroData.payload.doc.data(),
        });
      })
    })

  }


  
  public deleteLibro(documentId: any) {
    this.librosServi.deleteLibro(documentId).then(() => {
      console.log('Documento eliminado!');
    }, (error) => {
      console.error(error);
    });
  }
}
 
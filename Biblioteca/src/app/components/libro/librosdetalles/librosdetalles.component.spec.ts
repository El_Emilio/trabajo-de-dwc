import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibrosdetallesComponent } from './librosdetalles.component';

describe('LibrosdetallesComponent', () => {
  let component: LibrosdetallesComponent;
  let fixture: ComponentFixture<LibrosdetallesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibrosdetallesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibrosdetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LibrosComponent } from './libros/libros.component';
import { LibrosdetallesComponent } from './librosdetalles/librosdetalles.component';
import { NuevoLibroComponent } from './nuevo-libro/nuevo-libro.component';
import { LibrosEditComponent } from './libros-edit/libros-edit.component';

const routes:Routes=[
  {path:"", component:LibrosComponent},
  {path:"librodetalles/:id" ,component:LibrosdetallesComponent},
  {path:"nuevoLibro", component:NuevoLibroComponent},
  {path:"librosEdit/:id", component:LibrosEditComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibritosRoutingModule { }

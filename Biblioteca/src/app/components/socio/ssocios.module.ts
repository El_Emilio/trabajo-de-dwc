import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SsociosRoutingModule } from './ssocios-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SsociosRoutingModule
  ]
})
export class SsociosModule { }

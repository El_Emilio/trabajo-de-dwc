import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/login/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { SignInComponent } from './components/login/sign-in/sign-in.component';
import { SignUpComponent } from './components/login/sign-up/sign-up.component';
import { VerifyEmailComponent } from './components/login/verify-email/verify-email.component';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'dashboard', component: DashboardComponent , canActivate: [AuthGuard] , children:[
    {path:'home', component: HomeComponent , canActivate: [AuthGuard] },
    {path:"prestado",loadChildren:() =>import("./components/prestamo/prestado.module").then(m=>m.PrestadoModule), canActivate: [AuthGuard] },
    {path:"libritos",loadChildren:() =>import("./components/libro/libritos.module").then(m=>m.LibritosModule), canActivate: [AuthGuard] },
    {path:"ssocios",loadChildren:() =>import("./components/socio/ssocios.module").then(m=>m.SsociosModule), canActivate: [AuthGuard] },
  ]},
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },
 
 // {path:'', pathMatch:'full', redirectTo:'/sign-in'},
//{path:'**', pathMatch:'full', redirectTo:'/sign-in'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

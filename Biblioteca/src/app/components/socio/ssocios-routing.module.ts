import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SociosDetallesComponent } from './socios-detalles/socios-detalles.component';
import { SociosEditComponent } from './socios-edit/socios-edit.component';
import { SociosNuevoComponent } from './socios-nuevo/socios-nuevo.component';
import { SociosComponent } from './socios/socios.component';

const routes:Routes=[
  {path:"", component:SociosComponent},
  {path:"sociodetalles/:id" ,component:SociosDetallesComponent},
  {path:"nuevoSocio", component:SociosNuevoComponent},
  {path:"socioEdit/:id", component:SociosEditComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SsociosRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-socios-nuevo',
  templateUrl: './socios-nuevo.component.html',
  styleUrls: ['./socios-nuevo.component.css']
})
export class SociosNuevoComponent implements OnInit {

  constructor(private asf: SociosService, private route: Router) { }

  ngOnInit(): void {
  }

  public documentId = null;
  public currentStatus = 1;
  public nuevoSocioForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    apellidos: new FormControl('', Validators.required),
    foto: new FormControl('', Validators.required),
    edad: new FormControl('', Validators.required),
    correo: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    generofav: new FormControl('', Validators.required),
    tienelibro: new FormControl('false', Validators.required),

  });
  public newSocio(form: any, documentId = this.documentId) {
    console.log(`Status: ${this.currentStatus}`);
    if (this.currentStatus == 1) {
      let data = {
        nombre: form.nombre,
        apellidos: form.apellidos,
        foto: form.foto,
        edad: form.edad,
        correo: form.correo,
        telefono: form.telefono,
        generofav:form.generofav,
        tienelibro: false,

      }
      this.asf.createSocio(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.nuevoSocioForm.setValue({
          nombre: '',
          apellidos: '',
          foto: '',
          edad: '',
          correo: '',
          telefono: '',
          generofav: '',
          tienelibro: ''

        });
      }, (error) => {
        console.error(error);
      });
    } else {
      let data = {
        nombre: form.nombre,
        apellidos: form.apellidos,
        foto: form.foto,
        edad: form.edad,
        correo: form.correo,
        telefono: form.telefono,
        generofav:form.generofav,
        tienelibro: form.tienelibro,

      }
      this.asf.updateSocios(documentId, data).then(() => {
        this.currentStatus = 1;
        this.nuevoSocioForm.setValue({
          nombre: '',
          apellidos: '',
          foto: '',
          edad: '',
          correo: '',
          telefono: '',
          generofav: '',
          tienelibro: ''

        });
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });

    }
    this.route.navigate(["/dashboard/ssocios"])
  }

}

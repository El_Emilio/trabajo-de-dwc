import { style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,ParamMap, Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';

import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.css']
})
export class PrestamosComponent implements OnInit {

prestamoElegido:any;
  public prestamos:any=[];
  public documentId = "";
  public currentStatus = 1;
  public editaForm:any
  public libros:any=[];
  public socios:any=[];
  libroElegido: any;
  fechaFinal:Date = new Date();
  constructor(private prestamosServi:  PrestamosService, private rutita:ActivatedRoute, private libroServi:  LibrosService, private sociossServi:SociosService ) { }


  ngOnInit(): void {
    this.prestamosServi.getPrestamos().subscribe( (prestamoSnapshots)=>{
      this.prestamos=[];
      prestamoSnapshots.forEach((socioData: any)=>{
        this.prestamos.push({
          id:socioData.payload.doc.id,
          data:socioData.payload.doc.data(),
          
        });
        
      })
    })
    this.libroServi.getLibros().subscribe((libroSnapshot:any) => {
      this.libros = [];
      libroSnapshot.forEach((librosData: any) =>{
        this.libros.push({
          id: librosData.payload.doc.id,
          data: librosData.payload.doc.data()
        });
      });
    });
    this.sociossServi.getSocios().subscribe( (socioSnapshots)=>{
      this.socios=[];
      socioSnapshots.forEach((socioData: any)=>{
        this.socios.push({
          id:socioData.payload.doc.id,
          data:socioData.payload.doc.data(),
          
        });
        
      })
    })

  }

  public deletePrestamos(documentId: any, libroId:any) {
    
    let bucle =this.libroServi.getLibro(libroId).subscribe((libroData:any)=>{
      this.libroElegido = libroData.payload.data();
      console.log(this.libroElegido.prestado);
      this.libroElegido.prestado = false;
      this.libroServi.updateLibro(libroId, this.libroElegido);
      bucle.unsubscribe();
    })
    let bucle2 =this.prestamosServi.getPrestamo(documentId).subscribe((prestamoData:any)=>{
      this.prestamoElegido=prestamoData.payload.data();
      this.prestamoElegido.fechaFin = this.fechaFinal;
      console.log(this.prestamoElegido.fechaFin);
      this.prestamosServi.updatePrestamo(documentId, this.prestamoElegido);
      bucle2.unsubscribe();
    })
    
  }
  public ampliar(documentId:any){
    let fechaAmpliacion: Date = new Date();
    fechaAmpliacion.setDate(fechaAmpliacion.getDate() + 10);
    let ampliSub = this.prestamosServi.getPrestamo(documentId).subscribe((prestamoData:any)=>{
      this.prestamoElegido=prestamoData.payload.data();
      this.prestamoElegido.fechaFin=fechaAmpliacion;
      this.prestamosServi.updatePrestamo(documentId,this.prestamoElegido);
      ampliSub.unsubscribe();
    })
  }
 
  comparaciones(idLibroPrestamo:String, idLibro:String): Boolean{
    return Boolean (String (idLibroPrestamo.trim()) == String (idLibro.trim()))
  }
}

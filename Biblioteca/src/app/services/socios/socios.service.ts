import { Injectable } from '@angular/core';
import{AngularFirestore} from '@angular/fire/compat/firestore'

@Injectable({
  providedIn: 'root'
})
export class SociosService {

  constructor(private firestore: AngularFirestore) { }
  //Crea un nuevo socio
  public createSocio(data: {nombre: string, apellidos: string, foto:string, edad:number, correo:string,telefono:number ,generofav:string, tienelibro:boolean}) {
    return this.firestore.collection('socios').add(data);
  }
  //Obtiene un socio
  public getSocio(documentId: string) {
    return this.firestore.collection('socios').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los socios
  public getSocios() {
    return this.firestore.collection('socios').snapshotChanges();
  }
   //Actualiza un socios
   public updateSocios(documentId: any, data: any) {
    return this.firestore.collection('socios').doc(documentId).set(data);
  }
  //Actualiza un socio
  public updateSocio(documentId: string, data: any) {
    return this.firestore.collection('socios').doc(documentId).set(data);
  }

  //Eliminar un socio
  public deleteSocio(DocumentId:string){
    return this.firestore.collection("socios").doc(DocumentId).delete();
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-socios-detalles',
  templateUrl: './socios-detalles.component.html',
  styleUrls: ['./socios-detalles.component.css']
})
export class SociosDetallesComponent implements OnInit {
  public socio: any;
  public id?: any;
  public currentStatus = 1;
  constructor(private activatedRoute: ActivatedRoute, private socioservice: SociosService, private sociosServi: SociosService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => { this.id = params['id'] }); {
      this.sociosServi.getSocio(this.id).subscribe((socioData) => {
        this.socio = socioData.payload.data();
      })
    }
  }
}

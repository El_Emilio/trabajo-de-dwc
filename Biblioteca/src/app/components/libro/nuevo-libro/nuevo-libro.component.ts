import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';


@Component({
  selector: 'app-nuevo-libro',
  templateUrl: './nuevo-libro.component.html',
  styleUrls: ['./nuevo-libro.component.css']
})
export class NuevoLibroComponent implements OnInit {

  constructor(private asf: LibrosService, private route: Router) { }

  ngOnInit(): void {

  }

  public documentId = null;
  public currentStatus = 1;
  public nuevoLibroForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    autor: new FormControl('', Validators.required),
    foto: new FormControl('', Validators.required),
    isbn: new FormControl('', Validators.required),
    genero: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required),
    prestado: new FormControl('false', Validators.required),

  });
  public newLibro(form: any, documentId = this.documentId) {
    console.log(`Status: ${this.currentStatus}`);
    if (this.currentStatus == 1) {
      let data = {
        nombre: form.nombre,
        autor: form.autor,
        foto: form.foto,
        genero: form.genero,
        descripcion: form.descripcion,
        isbn: form.isbn,
        prestado: false,

      }
      this.asf.createLibro(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.nuevoLibroForm.setValue({
          nombre: '',
          autor: '',
          foto: '',
          genero: '',
          descripcion: '',
          isbn: '',
          prestado: '',

        });
      }, (error) => {
        console.error(error);
      });
    } else {
      let data = {
        nombre: form.nombre,
        autor: form.autor,
        foto: form.foto,
        genero: form.genero,
        descripcion: form.descripcion,
        isbn: form.isbn,
        prestado: form.prestado,

      }
      this.asf.updateLibros(documentId, data).then(() => {
        this.currentStatus = 1;
        this.nuevoLibroForm.setValue({
          nombre: '',
          autor: '',
          foto: '',
          genero: '',
          descripcion: '',
          isbn: '',
          prestado: '',

        });
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });

    }
    this.route.navigate(["/dashboard/libritos"])
  }

}

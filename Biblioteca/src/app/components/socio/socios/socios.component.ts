import { Component, OnInit } from '@angular/core';
import { SociosService } from 'src/app/services/socios/socios.service';
import { SsociosModule } from '../ssocios.module';

@Component({
  selector: 'app-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.css']
})
export class SociosComponent implements OnInit {

  public socios:any=[];
  public documentId = "";
  public currentStatus = 1;
  constructor(private sociosServi:  SociosService ) { }


  ngOnInit(): void {
    this.sociosServi.getSocios().subscribe( (socioSnapshots)=>{
      this.socios=[];
      socioSnapshots.forEach((socioData: any)=>{
        this.socios.push({
          id:socioData.payload.doc.id,
          data:socioData.payload.doc.data(),
        });
      })
    })
  }

  public deleteSocio(documentId: any) {
    this.sociosServi.deleteSocio(documentId).then(() => {
      console.log('Documento eliminado!');
    }, (error) => {
      console.error(error);
    });
  }
}

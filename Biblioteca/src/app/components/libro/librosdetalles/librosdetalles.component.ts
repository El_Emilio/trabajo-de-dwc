import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-librosdetalles',
  templateUrl: './librosdetalles.component.html',
  styleUrls: ['./librosdetalles.component.css']
})
export class LibrosdetallesComponent implements OnInit {
  public libro: any;
  public id?: any;
  public currentStatus = 1;
  constructor(private activatedRoute: ActivatedRoute, private libroservice: LibrosService, private librosServi: LibrosService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => { this.id = params['id'] }); {
      this.librosServi.getLibro(this.id).subscribe((libroData) => {
        this.libro = libroData.payload.data();
      })
    }
  }
  
}

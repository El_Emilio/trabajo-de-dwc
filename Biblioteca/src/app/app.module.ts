import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LibrosComponent } from './components/libro/libros/libros.component';

import { SociosComponent } from './components/socio/socios/socios.component';
import { FooterComponent } from './components/footer/footer.component';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { NuevoLibroComponent } from './components/libro/nuevo-libro/nuevo-libro.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LibrosdetallesComponent } from './components/libro/librosdetalles/librosdetalles.component';
import { SociosEditComponent } from './components/socio/socios-edit/socios-edit.component';
import { SociosDetallesComponent } from './components/socio/socios-detalles/socios-detalles.component';
import { SociosNuevoComponent } from './components/socio/socios-nuevo/socios-nuevo.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { SignInComponent } from './components/login/sign-in/sign-in.component';
import { SignUpComponent } from './components/login/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/login/verify-email/verify-email.component';
import { DashboardComponent } from './components/login/dashboard/dashboard.component';
// Mi servicio de autenticación
import { AuthService } from "./services/auth/auth.service";




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,

    NavbarComponent,
    LibrosComponent,
    SociosComponent,
    FooterComponent,
    NuevoLibroComponent,
    LibrosdetallesComponent,
    SociosEditComponent,
    SociosDetallesComponent,
    SociosNuevoComponent,
    DashboardComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent
    
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    Ng2SearchPipeModule
   
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }

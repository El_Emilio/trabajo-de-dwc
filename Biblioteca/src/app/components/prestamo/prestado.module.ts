import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestadoRoutingModule } from './prestado-routing.module';
import { PrestamosnuevoComponent } from './prestamosnuevo/prestamosnuevo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrestamosComponent } from './prestamos/prestamos.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    PrestamosnuevoComponent,
    PrestamosComponent
  ],
  imports: [
    CommonModule,
    PrestadoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule
    
  ]
})
export class PrestadoModule { }
